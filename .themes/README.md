# DARK-UNICORNS! notification theme

## How to install

Simply place the desired theme in `~/.themes` or as root in `usr/share/themes`

## Screenshots
#### DARK-BLUE-UNICORNS!
![demo1](/.themes/DARK-BLUE-UNICORNS!/DARK-BLUE-UNICORNS!.png)
#### DARK-GREEN-UNICORNS!
![demo2](/.themes/DARK-GREEN-UNICORNS!/DARK-GREEN-UNICORNS!.png)
#### DARK-JOKER-UNICORNS!
![demo3](/.themes/DARK-JOKER-UNICORNS!/DARK-JOKER-UNICORNS!.png)
#### DARK-PURPLE-UNICORNS!
![demo4](/.themes/DARK-PURPLE-UNICORNS!/DARK-PURPLE-UNICORNS!.png)
#### DARK-RED-UNICORNS!
![demo5](/.themes/DARK-RED-UNICORNS!/DARK-RED-UNICORNS!.png)
