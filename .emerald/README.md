# My Emerald Themes

## How to install

Simply place the desired theme in `~/.emerald/themes`

## Screenshots
#### BLUE-UNICORNS!
![demo1](.emerald/themes/BLUE-UNICORNS!/theme.screenshot.png)
#### GREEN-UNICORNS!
![demo2](.emerald/themes/GREEN-UNICORNS!/theme.screenshot.png)
#### NO-UNICORNS!
![demo3](.emerald/themes/NO-UNICORNS!/theme.screenshot.png)
#### PURPLE-UNICORNS!
![demo4](.emerald/themes/PURPLE-UNICORNS!/theme.screenshot.png)
#### RED-UNICORNS!
![demo5](.emerald/themes/RED-UNICORNS!/theme.screenshot.png)
