# My Dotfiles

## My Themes
- [DARK-UNICORNS! notification themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.themes)
- [UNICORNS! emerald themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.emerald)
- [System conky themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/conky)
- [DARK-UNICORNS! rofi themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/rofi)
- [DARK-UNICORNS! spicetify themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/spicetify)
- [DARK-UNICORNS! tint2 themes](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/tint2)

## [Config Files](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config)
- [zsh](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/zsh)
- [Ranger](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/ranger)
- [Alacritty](https://gitlab.com/IM-OUT-OF-ORDER/dotfiles/-/tree/master/.config/alacritty)
