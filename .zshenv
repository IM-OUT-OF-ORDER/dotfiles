typeset -U PATH path

# Other XDG Paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}

# Disable Files
export LESSHISTFILE=-

# Fixing Paths
export XINITRC="$XDG_CONFIG_HOME"/X11/xinitrc
export ZDOTDIR=$HOME/.config/zsh
export HISTFILE="$XDG_CONFIG_HOME"/zsh/.zsh_history
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GOPATH="$XDG_DATA_HOME"/go
export DVDCSS_CACHE="$XDG_DATA_HOME"/dvdcss
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority

# Default Apps
export EDITOR="geany"
export TERMINAL="alacritty"
export VIDEO="mpv"
export BROWSER="firefox"
