# ███████╗███████╗██╗  ██╗██████╗  ██████╗
# ╚══███╔╝██╔════╝██║  ██║██╔══██╗██╔════╝
#   ███╔╝ ███████╗███████║██████╔╝██║     
#  ███╔╝  ╚════██║██╔══██║██╔══██╗██║     
# ███████╗███████║██║  ██║██║  ██║╚██████╗
# ╚══════╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝

# History
HISTFILE=~/.config/zsh/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

# Parameters
setopt autocd beep extendedglob nomatch notify INC_APPEND_HISTORY HIST_IGNORE_ALL_DUPS HIST_REDUCE_BLANKS

# Auto/Tab Complete
autoload -Uz compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)				# Include hidden files

# Plugins
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

### ADD-ONS ###

# Neofetch | Lolcat
neofetch | lolcat -a -s 1000.0 -F 0.12 

# Color Script
/run/media/unknown/Games/Scripts/shell-color-scripts/.colorscript -r

# LS Colors
eval $(dircolors "$XDG_CONFIG_HOME"/.dir_colors)

# Better Yaourt Colors
export YAOURT_COLORS="nb=1:pkg=1:ver=1;32:lver=1;45:installed=1;42:grp=1;34:od=1;41;5:votes=1;44:dsc=0:other=1;35"

# Custom Scripts
export PATH=$PATH:/run/media/unknown/Games/Scripts/

# Bash Insulter
[ -f /run/media/unknown/Games/Scripts/.bash_insulter ] && . /run/media/unknown/Games/Scripts/.bash_insulter

# Aliases
[ -f "$HOME/.config/.aliasrc" ] && source "$HOME/.config/.aliasrc"
