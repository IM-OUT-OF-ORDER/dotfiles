--[[
#####################
##   Blue-System   ##
#####################
]]

conky.config = {

	--Various settings

	background = true, 					                                    	-- forked to background
	cpu_avg_samples = 2,					                                 	-- The number of samples to average for CPU monitoring.
	diskio_avg_samples = 10,		        	                        		-- The number of samples to average for disk I/O monitoring.
	double_buffer = true,				        		                        -- Use the Xdbe extension? (eliminates flicker)
	if_up_strictness = 'address',				                                -- how strict if testing interface is up - up, link or address
	net_avg_samples = 2,						                                -- The number of samples to average for net data
	no_buffers = true,				                                         	-- Subtract (file system) buffers from used memory?
	temperature_unit = 'celsius',				                                -- fahrenheit or celsius
	text_buffer_size = 2048,				        	                        -- size of buffer for display of content of large variables - default 256
	update_interval = 1,						                                -- update interval
	imlib_cache_size = 0,                                                       -- disable image cache to get a new spotify cover per song
	                                                                            
	--Placement                                                                 
                                                                                            
	alignment = 'top_left',   			                                        -- top_left,top_middle,top_right,bottom_left,bottom_middle,bottom_right,
									                                            -- middle_left,middle_middle,middle_right,none
                                                            
	gap_x = 2575,	    				                                    	-- pixels between right or left border
	gap_y = 660,	   				        		                            -- pixels between bottom or left border
	minimum_height = 1080,			         			                        -- minimum height of window
	minimum_width = 320,		         				                        -- minimum height of window
	maximum_width = 320,	           				                        	-- maximum height of window
                                                                                
	--Graphical                                                                 
                                                                                
	border_inner_margin = 10, 	                                   				-- margin between border and text
	border_outer_margin = 5,        		                        			-- margin between border and edge of window
	border_width = 0, 					                                      	-- border width in pixels
	default_bar_width = 80,				                                    	-- default is 0 - full width
	default_bar_height = 10,				                                  	-- default is 6
	default_gauge_height = 25,	                                 				-- default is 25
	default_gauge_width =40,			                                		-- default is 40
	default_graph_height = 40,			                                   		-- default is 25
	default_graph_width = 0,			           	                         	-- default is 0 - full width
	default_shade_color = '#000000',			                                -- default shading colour
	default_outline_color = '#000000',			                                -- default outline colour
	draw_borders = false,					                                  	-- draw borders around text
	draw_graph_borders = true,				                                	-- draw borders around graphs
	draw_shades = false,						                                -- draw shades
	draw_outline = false,						                                -- draw outline
	stippled_borders = 0,						                                -- dashing the border
                                                                                
	--Textual                                                                   
                                                                                
	extra_newline = false,						                                -- extra newline at the end - for asesome's wiboxes
	format_human_readable = true,				                                -- KiB, MiB rather then number of bytes
	font = 'mononoki Nerd Font:size=10',  				                        -- font for complete conky unless in code defined
	max_text_width = 0,					        	                            -- 0 will make sure line does not get broken if width too smal
	max_user_text = 16384,						                                -- max text in conky default 16384
	override_utf8_locale = true,				                                -- force UTF8 requires xft
	short_units = true,						                                    -- shorten units from KiB to k
	top_name_width = 21,						                                -- width for $top name value default 15
	top_name_verbose = false,					                                -- If true, top name shows the full command line of  each  process - Default value is false.
	uppercase = false,						                                    -- uppercase or not
	use_spacer = 'none',						                                -- adds spaces around certain objects to align - default none
	use_xft = true,							                                    -- xft font - anti-aliased font
	xftalpha = 1,							                                    -- alpha of the xft font - between 0-1
                                                                                
	--Windows                                                                   
                                                                                
	own_window = true,						                                    -- create your own window to draw
	own_window_argb_value = 255,   		                	                    -- real transparency - composite manager required 0-255
	own_window_argb_visual = true,				                                -- use ARGB - composite manager required
	own_window_colour = '#1C1C1C',				                                -- set colour if own_window_transparent no
	own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',      -- if own_window true - just hints - own_window_type sets it
	own_window_transparent = false,				                                -- if own_window_argb_visual is true sets background opacity 0%
	own_window_title = 'system_conky',			                                -- set the name manually  - default conky "hostname"
	own_window_type = 'normal',			                                        -- if own_window true options are: normal/override/dock/desktop/panel
                                                                                
                                                                                
	--Colours                                                                   
                                                                                
	default_color = '#FFFFFF',
	color1 = '#2C89A0',
	color2 = '#204760',
	color3 = '#4285f4',
	color4 = '#99ccff',
	color5 = '#00ffff',
	
    --Lua


};

conky.text = [[
#*--------------*
# DATE & TIME   |
#*--------------*
${color5}${voffset -7}${font mononoki Nerd Font:size=12}${alignc}${time %A}${font}
${color3}${font mononoki Nerd Font:size=36}${alignc}${time %H}${color4}${time %M}${font}
${color5}${font mononoki Nerd Font:size=12}${alignc}${time %d} of ${time %B}, ${time %Y}${font}
${color5}${font mononoki Nerd Font:size=12}${alignc}${time %d}-${time %m}-${time %Y}${font}

#*--------------*
# SYSTEM        |
#*--------------*
${color2}${font mononoki Nerd Font bold:size=12}S Y S T E M ${color2}${font}${stippled_hr 2}
${color1}Hostname:${color}${alignr}${nodename}
${color1}Distro:${color}${alignr}${execi 6000 lsb_release -a | grep 'Description'|awk {'print $2""$3""$4""$5'}} ${execi 6000 lsb_release -a | grep 'Release'|awk {'print $2""$3""$4""$5'}} ${execi 6000 lsb_release -a | grep 'Codename'|awk {'print $2""$3""$4""$5'}}
${color1}Kernel:${color}${alignr}${exec uname -r}
${color1}CPU:${color}${alignr}${execi 1000 cat /proc/cpuinfo | grep 'model name' | sed -e 's/model name.*: //'| uniq | cut -c 1-26}
${color1}Nvidia:${color}${alignr}${execp nvidia-smi --query-supported-clocks=gpu_name --format=csv,noheader}
${color1}Nvidia Driver:${color}${alignr}${execi 60000 nvidia-smi | grep "Driver Version"| awk {'print $3'}}
${color1}Uptime:${color}${alignr}${uptime}

#*--------------*
# PROCESSORS    |
#*--------------*
${color2}${font mononoki Nerd Font bold:size=12}P R O C E S S O R S ${color2}${font}${stippled_hr 2}
${color1}CPU Clock:${color}${alignr}${freq}MHz
${color1}Core 0:${color}${alignr}${execi 10 sensors | grep 'Core 0' | awk {'print $3'}}
${color1}Core 1:${color}${alignr}${execi 10 sensors | grep 'Core 1' | awk {'print $3'}}
${color1}Core 2:${color}${alignr}${execi 10 sensors | grep 'Core 2' | awk {'print $3'}}
${color1}Core 3:${color}${alignr}${execi 10 sensors | grep 'Core 3' | awk {'print $3'}}
${color1}History:${color}${alignr}${color3}${cpugraph 8,100}
${color1}${offset 30}Core 0:${color}${alignr}${offset -10}${cpu cpu1}%${alignr}${color3}${cpubar cpu1}
${color1}${offset 30}Core 1:${color}${alignr}${offset -10}${cpu cpu2}%${alignr}${color3}${cpubar cpu2}
${color1}${offset 30}Core 2:${color}${alignr}${offset -10}${cpu cpu3}%${alignr}${color3}${cpubar cpu3}
${color1}${offset 30}Core 3:${color}${alignr}${offset -10}${cpu cpu4}%${alignr}${color3}${cpubar cpu4}
${color5}Top Processes${color5}${goto 236}cpu%${goto 298}mem%
${color3}${offset 30}1:${color4} ${top name 1}${color}${alignr}${goto 222}${top cpu 1}${goto 284}${top mem 1}
${color3}${offset 30}2:${color4} ${top name 2}${color}${alignr}${goto 222}${top cpu 2}${goto 284}${top mem 2}
${color3}${offset 30}3:${color4} ${top name 3}${color}${alignr}${goto 222}${top cpu 3}${goto 284}${top mem 3}
${color3}${offset 30}4:${color4} ${top name 4}${color}${alignr}${goto 222}${top cpu 4}${goto 284}${top mem 4}
${color3}${offset 30}5:${color4} ${top name 5}${color}${alignr}${goto 222}${top cpu 5}${goto 284}${top mem 5}

#*--------------*
# MEMORY        |
#*--------------*
${color2}${font mononoki Nerd Font bold:size=12}M E M O R Y ${color2}${font}${stippled_hr 2}
${color1}${offset 30}RAM:${color}${alignr}${offset -10}${mem} / ${memmax}${alignr}${color3}${membar}
${color1}${offset 30}Swap:${color}${alignr}${offset -10}${swap} / ${swapmax}${alignr}${color3}${swapbar}
${color5}Top Processes${color5}${goto 236}cpu%${goto 298}mem%
${color3}${offset 30}1:${color4} ${top_mem name 1}${color}${alignr}${goto 222}${top_mem cpu 1}${goto 284}${top_mem mem 1}
${color3}${offset 30}2:${color4} ${top_mem name 2}${color}${alignr}${goto 222}${top_mem cpu 2}${goto 284}${top_mem mem 2}
${color3}${offset 30}3:${color4} ${top_mem name 3}${color}${alignr}${goto 222}${top_mem cpu 3}${goto 284}${top_mem mem 3}
${color3}${offset 30}4:${color4} ${top_mem name 4}${color}${alignr}${goto 222}${top_mem cpu 4}${goto 284}${top_mem mem 4}
${color3}${offset 30}5:${color4} ${top_mem name 5}${color}${alignr}${goto 222}${top_mem cpu 5}${goto 284}${top_mem mem 5}

#*--------------*
# DRIVES        |
#*--------------*
${color2}${font mononoki Nerd Font bold:size=12}D R I V E S ${color2}${font}${stippled_hr 2}
${color5}Root:${color}${alignr}${offset -10}${fs_used /} / ${fs_size /}${alignr}${color3}${fs_bar /}
${color1}${offset 30}I/O Read:${color}${alignr}${offset -10}${diskio_read /dev/sda4}${alignr}${color3}${diskiograph_read sda4 8,100}
${color1}${offset 30}I/O Write:${color}${alignr}${offset -10}${diskio_write /dev/sda4}${alignr}${color3}${diskiograph_write sda4 8,100}

${color5}Home:${color}${alignr}${offset -10}${fs_used /home} / ${fs_size /home}${alignr}${color3}${fs_bar /home}
${color1}${offset 30}I/O Read:${color}${alignr}${offset -10}${diskio_read /dev/sda2}${alignr}${color3}${diskiograph_read sda2 8,100}
${color1}${offset 30}I/O Write:${color}${alignr}${offset -10}${diskio_write /dev/sda2}${alignr}${color3}${diskiograph_write sda2 8,100}

${color5}Games:${color}${alignr}${offset -10}${fs_used /run/media/unknown/Games} / ${fs_size /run/media/unknown/Games}${alignr}${color3}${fs_bar /run/media/unknown/Games}
${color1}${offset 30}I/O Read:${color}${alignr}${offset -10}${diskio_read /dev/sdb1}${alignr}${color3}${diskiograph_read sdb1 8,100}
${color1}${offset 30}I/O Write:${color}${alignr}${offset -10}${diskio_write /dev/sdb1}${alignr}${color3}${diskiograph_write sdb1 8,100}

${color5}Movies:${color}${alignr}${offset -10}${fs_used /run/media/unknown/Movies} / ${fs_size /run/media/unknown/Movies}${alignr}${color3}${fs_bar /run/media/unknown/Movies}
${color1}${offset 30}I/O Read:${color}${alignr}${offset -10}${diskio_read /dev/sdd1}${alignr}${color3}${diskiograph_read sdd1 8,100}
${color1}${offset 30}I/O Write:${color}${alignr}${offset -10}${diskio_write /dev/sdd1}${alignr}${color3}${diskiograph_write sdd1 8,100}

#*--------------*
# NETWORK       |
#*--------------*
${color2}${font mononoki Nerd Font bold:size=12}N E T W O R K ${color2}${font}${stippled_hr 2}
${color5}IP Address: ${color}${alignr}${offset -10$}${addrs enp0s31f6}
${color1}${offset 30}Eth Up:${color}${alignr}${offset -10$}${upspeed enp0s31f6}${alignr}${color3}${upspeedgraph enp0s31f6 8,100}
${color1}${offset 30}Eth Down:${color}${alignr}${offset -10$}${downspeed enp0s31f6}${alignr}${color3}${downspeedgraph enp0s31f6 8,100}

#*--------------*
# NVIDIA        |
#*--------------*
#${color2}${font mononoki Nerd Font bold:size=12}N V I D I A ${color2}${font}${stippled_hr 2}
#${color1}${offset 30}GPU Temp:${color}${alignr}${nvidia temp}°C
#${color1}${offset 30}Fan Speed:${color}${alignr}${exec nvidia-smi -i 0| grep % | cut -c 3-5}
#${color1}${offset 30}GPU Clock:${color}${alignr}${execi 60 nvidia-settings -q GPUCurrentClockFreqs | grep -m 1 Attribute | awk '{print $4}' | sed -e 's/\.//' | cut -d, -f1}MHz
#${color1}${offset 30}Utilization:${color}${alignr}${exec nvidia-smi -i 0 | grep % | cut -c 61-63}%
#${color1}${offset 30}VRAM Utilization:${color}${alignr}${exec nvidia-smi -i 0| grep % | cut -c 37-54}
]];
