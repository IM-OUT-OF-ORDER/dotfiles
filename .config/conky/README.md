# My Conky Themes

## How to install

Simply place the desired theme in `~/.config/conky`

Run this command:

In **Terminal**:
```bash
conky -c ~/.config/conky/Purple-System.conkyrc
```
## Screenshots
#### Blue-System
![demo1](.config/conky/Blue-System.png)
#### Green-System
![demo2](.config/conky/Green-System.png)
#### Purple-System
![demo3](.config/conky/Purple-System.png)
#### Red-System
![demo4](.config/conky/Red-System.png)
