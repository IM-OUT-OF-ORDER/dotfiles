# DARK-UNICORNS! tint2 theme

## How to install

Simply place the desired theme in `~/.config/tint2`

## Screenshots
#### DARK-BLUE-UNICORNS!
![demo1](DARK-BLUE-UNICORNS!.png)
#### DARK-GREEN-UNICORNS!
![demo2](DARK-GREEN-UNICORNS!.png)
#### DARK-PURPLE-UNICORNS!
![demo3](DARK-PURPLE-UNICORNS!.png)
#### DARK-RED-UNICORNS!
![demo4](DARK-RED-UNICORNS!.png)
