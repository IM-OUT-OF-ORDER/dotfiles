# Configuration file for dircolors, a utility to help you set the
# LS_COLORS environment variable used by GNU ls with the --color option.

# Copyright (C) 1996-2014 Free Software Foundation, Inc.
# Copying and distribution of this file, with or without modification,
# are permitted provided the copyright notice and this notice are preserved.

# The keywords COLOR, OPTIONS, and EIGHTBIT (honored by the
# slackware version of dircolors) are recognized but ignored.

# You can copy this file to .dir_colors in your $HOME directory to override
# the system defaults.

# Below, there should be one TERM entry for each termtype that is colorizable
TERM Eterm
TERM ansi
TERM color-xterm
TERM con132x25
TERM con132x30
TERM con132x43
TERM con132x60
TERM con80x25
TERM con80x28
TERM con80x30
TERM con80x43
TERM con80x50
TERM con80x60
TERM cons25
TERM console
TERM cygwin
TERM dtterm
TERM eterm-color
TERM gnome
TERM gnome-256color
TERM hurd
TERM jfbterm
TERM konsole
TERM kterm
TERM linux
TERM linux-c
TERM mach-color
TERM mach-gnu-color
TERM mlterm
TERM putty
TERM putty-256color
TERM rxvt
TERM rxvt-256color
TERM rxvt-cygwin
TERM rxvt-cygwin-native
TERM rxvt-unicode
TERM rxvt-unicode-256color
TERM rxvt-unicode256
TERM screen
TERM screen-256color
TERM screen-256color-bce
TERM screen-bce
TERM screen-w
TERM screen.Eterm
TERM screen.rxvt
TERM screen.linux
TERM st
TERM st-256color
TERM terminator
TERM vt100
TERM xterm
TERM xterm-16color
TERM xterm-256color
TERM xterm-88color
TERM xterm-color
TERM xterm-debian
TERM alacritty

# Below are the color init strings for the basic file types. A color init
# string consists of one or more of the following numeric codes:
# Attribute codes:
# 00=none 01=bold 04=underscore 05=blink 07=reverse 08=concealed
# Text color codes:
# 30=black 31=red 32=green 33=yellow 34=blue 35=magenta 36=cyan 37=white
# Background color codes:
# 40=black 41=red 42=green 43=yellow 44=blue 45=magenta 46=cyan 47=white
#NORMAL 00	                   # no color code at all
#FILE 00	                   # regular file: use no color at all
RESET 0		                   # reset to "normal" color
DIR 01;04;30;47                # directory
LINK 01;33	                   # symbolic link.  (If you set this to 'target' instead of a numerical value, the color is as for the file pointed to.)
MULTIHARDLINK 00	           # regular file with more than one link
FIFO 33;40	                   # pipe
SOCK 01;35	                   # socket
DOOR 01;35	                   # door
BLK 01;33;40	               # block device driver
CHR 01;33;40	               # character device driver
ORPHAN 01;05;30;41             # orphaned symlinks
MISSING 01;05;30;41            # ... and the files they point to
SETUID 37;41	               # file that is setuid (u+s)
SETGID 30;43	               # file that is setgid (g+s)
CAPABILITY 30;41	           # file with capability
STICKY_OTHER_WRITABLE 30;42    # dir that is sticky and other-writable (+t,o+w)
OTHER_WRITABLE 34;42           # dir that is other-writable (o+w) and not sticky
STICKY 37;44               	   # dir with the sticky bit set (+t) and not other-writable

# This is for files with execute permission:
EXEC 01;30;46

# List any file extensions like '.gz' or '.tar' that you would like ls
# to colorize below. Put the extension, a space, and the color init string.
# (and any comments you want to add after a '#')

# If you use DOS-style suffixes, you may want to uncomment the following:
#.cmd 01;32 # executables (bright green)
.exe 01;30;46
#.com 01;32
#.btm 01;32
.bat 01;30;46
# Or if you want to colorize scripts even if they do not have the
# executable bit actually set.
#.sh  01;32
#.csh 01;32

 # archives or compressed (bright red)
.tar 01;31
.tgz 01;31
.arc 01;31
.arj 01;31
.taz 01;31
.lha 01;31
.lz4 01;31
.lzh 01;31
.lzma 01;31
.tlz 01;31
.txz 01;31
.tzo 01;31
.t7z 01;31
.zip 01;31
.z   01;31
.Z   01;31
.dz  01;31
.gz  01;31
.lrz 01;31
.lz  01;31
.lzo 01;31
.xz  01;31
.bz2 01;31
.bz  01;31
.tbz 01;31
.tbz2 01;31
.tz  01;31
.deb 01;31
.rpm 01;31
.jar 01;31
.war 01;31
.ear 01;31
.sar 01;31
.rar 01;31
.alz 01;31
.ace 01;31
.zoo 01;31
.cpio 01;31
.7z  01;31
.rz  01;31
.cab 01;31
.pkg 01;31
.apk 01;31

# image formats
.jpg 01;35
.jpeg 01;35
.gif 01;35
.bmp 01;35
.pbm 01;35
.pgm 01;35
.ppm 01;35
.tga 01;35
.xbm 01;35
.xpm 01;35
.tif 01;35
.tiff 01;35
.png 01;35
.svg 01;35
.svgz 01;35
.mng 01;35
.pcx 01;35
.m2v 01;35
.webm 01;35
.ogm 01;35
.qt  01;35
.nuv 01;35
.asf 01;35
.rmvb 01;35
.flc 01;35
.fli 01;35
.gl 01;35
.dl 01;35
.xcf 01;35
.xwd 01;35
.yuv 01;35
.cgm 01;35
.emf 01;35
.ico 01;35

# http://wiki.xiph.org/index.php/MIME_Types_and_File_Extensions
.axv 00;35
.anx 00;35
.ogv 00;35
.ogx 00;35
.axa 00;35
.oga 00;35
.spx 00;35
.xspf 00;35

# document files
.pdf 01;32
.ps 01;32
.txt 01;32
.log 01;32
.tex 01;32
.doc 01;32
.docx 01;32
.bash_history 01;32
.md 01;32
.zsh_history 01;32

# config files
.conf 01;30;45
.ini 01;30;45
.conkyrc 01;30;45
.config 01;30;45
.dir_colors 01;30;45
.yml 01;30;45
.json 01;30;45
.cfg 01;30;45
.nanorc 01;30;45
.aliasrc 01;30;45
.zshenv 01;30;45
.rasi 01;30;45
.tint2rc 01;30;45
.zprofile 01;30;45

# audio formats
.aac 00;36
.au 00;36
.flac 00;36
.m4a 00;36
.mid 00;36
.midi 00;36
.mka 00;36
.mp3 00;36
.mpc 00;36
.ogg 00;36
.ra 00;36
.wav 00;36
.wma 00;36

# video formats
.mp4 04;36
.mp4v 04;36
.TS 04;36
.avi 04;36
.mkv 04;36
.flv 04;36
.mov 04;36
.m4v 04;36
.mpg 04;36
.mpeg 04;36
.rm  04;36
.wmv 04;36
.vob 04;36
.h264 04;36

# fonts
.fnt 01;31;43
.fon 01;31;43
.otf 01;31;43
.ttf 01;31;43

# web 04;37
.htm 04;37
.html 04;37
.shtml 04;37
.xhtml 04;37

# images
.bin 30;42
.dmg 30;42
.img 30;42
.iso 30;42
.toast 30;42
.vcd 30;42

# desktop
.desktop 01;04;31;47

# programming
.patch 01;30;44
.diff 01;30;44
.py 01;30;44
.bash_insulter 01;30;44
.colorscript 01;30;44
.bashrc 01;30;44
.sh 01;30;44
.bash 01;30;44
.zshrc 01;30;44
.zsh 01;30;44
.fish 01;30;44
.vim 01;30;44
.bashrc.save 01;30;44
