{
    "_": "", 
    "buttons": {
        "A": {
            "action": "button(Keys.KEY_K)", 
            "name": "Cross"
        }, 
        "B": {
            "action": "button(Keys.KEY_L)", 
            "name": "Circle"
        }, 
        "BACK": {
            "action": "button(Keys.KEY_V)", 
            "name": "Select"
        }, 
        "C": {
            "action": "hold(menu('Default.menu'), menu('Default.menu'))"
        }, 
        "LB": {
            "action": "button(Keys.KEY_W)", 
            "name": "L1"
        }, 
        "RB": {
            "action": "button(Keys.KEY_P)", 
            "name": "R1"
        }, 
        "RGRIP": {
            "action": "button(Keys.KEY_O)", 
            "name": "R3"
        }, 
        "START": {
            "action": "button(Keys.KEY_N)", 
            "name": "Start"
        }, 
        "X": {
            "action": "button(Keys.KEY_J)", 
            "name": "Square"
        }, 
        "Y": {
            "action": "button(Keys.KEY_I)", 
            "name": "Triangle"
        }
    }, 
    "cpad": {}, 
    "gyro": {}, 
    "is_template": false, 
    "menus": {}, 
    "pad_left": {
        "action": "dpad(button(Keys.KEY_E), button(Keys.KEY_D), button(Keys.KEY_S), button(Keys.KEY_F))"
    }, 
    "pad_right": {}, 
    "stick": {
        "action": "dpad(button(Keys.KEY_R), button(Keys.KEY_G), button(Keys.KEY_T), button(Keys.KEY_H))", 
        "name": "Joystick"
    }, 
    "trigger_left": {
        "action": "trigger(254, 255, button(Keys.KEY_A))", 
        "name": "L2"
    }, 
    "trigger_right": {
        "action": "trigger(254, 255, button(Keys.KEY_SEMICOLON))", 
        "name": "R2"
    }, 
    "version": 1.4
}